import pyautogui

print("Posicione o cursor na janela da planilha de excel")

x = -1
while (x <= 0 or x > 1000) :
	print("Digite quantos campos deseja copiar")
	a = input('').split(" ")[0]
	x = int(a)

y = -1
while (y <= 0 or y > 1000) :
	print("Digite quantos tabs para pular um campo")
	a = input('').split(" ")[0]
	y = int(a)


pyautogui.keyDown('alt')
pyautogui.press('tab')
pyautogui.press('tab')
pyautogui.keyUp('alt')

pyautogui.keyDown('alt')
pyautogui.press('tab')
pyautogui.press('tab')
pyautogui.keyUp('alt')

pyautogui.hotkey('alt', 'tab')

for i in range(x):

	pyautogui.hotkey('alt', 'tab')
	pyautogui.hotkey('ctrl', 'c')
	pyautogui.press('down')

	pyautogui.hotkey('alt', 'tab')
	pyautogui.hotkey('ctrl', 'v')
	for j in range(y):
		pyautogui.press('tab')
